export function initTawkto() {

    var Tawk_API = Tawk_API || {},
        Tawk_LoadStart = new Date();
    var s1 = document.createElement("script"),
        s0 = document.getElementsByTagName("script")[0];
    s1.async = true;
    s1.src = 'https://embed.tawk.to/60dce9857f4b000ac03a6d35/1f9ff0dgd';
    s1.charset = 'UTF-8';
    s1.setAttribute('crossorigin', '*');
    s0.parentNode.insertBefore(s1, s0);
}