"use strict";

import './store/store';
import 'alpinejs';
import { env } from './libs/utils/constants';
import { initPageLoader } from './libs/components/pageloader';
import { switchDemoImages, insertBgImages, initModals } from './libs/utils/utils';
import { initNavbar } from './libs/components/navbar';
import { initSidebar } from './libs/components/sidebar';
import { initBackToTop } from './libs/components/backtotop';
import { initTypeform } from './libs/components/typeform';
import { initTawkto } from './libs/components/tawkto';

const feather = require('feather-icons');

window.initNavbar = initNavbar;
window.initSidebar = initSidebar;
window.initBackToTop = initBackToTop;

const showPageloader = initPageLoader();
const initializeTypeform = initTypeform();
// const initializeTawkto = initTawkto();

document.onreadystatechange = function () {
    if (document.readyState == 'complete') {

        //Switch demo images
        const changeImages = switchDemoImages(env);

        //Switch backgrounds
        const changeBackgrounds = insertBgImages();

        //Feather Icons
        const featherIcons = feather.replace();
        
        // Add modal windows
        const modals = initModals();
    }
}

